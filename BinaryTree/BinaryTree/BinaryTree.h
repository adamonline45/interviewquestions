#pragma once

#include "Node.h"

// Every node is either red or black.
// Every leaf(NULL) is black.
// If a node is red, then both its children are black.
// Every simple path from a node to a descendant leaf contains the same number of black nodes.

template <class T>
class BinaryTree
{
private:
	Node<T> * RootNode;
public:
	BinaryTree();
	void Insert(T element);
	void Remove(T element);
};

template<class T>
BinaryTree<T>::BinaryTree()
{
	RootNode = new Node<T>();
}

template<class T >
void BinaryTree<T>::Insert(T element)
{
	
}

template<class T>
void BinaryTree<T>::Remove(T element)
{

}
