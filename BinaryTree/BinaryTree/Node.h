#pragma once

template <class T>
class Node
{
private:
	Node<T> * L;
	Node<T> * R;
public:
	T Element;
};