#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

// This is a simple implementation. Ask me how I would improve this code!

int main()
{
	int cents;

	cout << "Enter a number of cents to get an efficient set of coins with that value: ";
	cin >> cents;
	cout << endl;

	int coinValues[] = { 100, 50, 25, 10, 5, 1 };	// Desc., ensure '1' exists!
	string coinNames[] = { "Dollars", "Half Dollars", "Quarters", "Dimes", "Nickels", "Pennies" };

	for (int i = 0; i < 6; i++)
	{
		int count = 0;

		while (cents >= coinValues[i])
		{
			cents -= coinValues[i];
			count++;
		}

		cout << setw(12) << coinNames[i] << ": " << count << endl;
	}

	cout << endl << "Press <Enter> to exit...";
	cin.get();
	cin.get();
}
