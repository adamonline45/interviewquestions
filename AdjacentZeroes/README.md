# AdjacentZeroes

This application will find clusters of a target value in a  matrix of values, uniquely coloring each cluster.

Currently windows-only, because of the console-coloring function.

## Sample Output
Note that each cluster of '0's are colored differently (though some are very similar).

![Sample output](readme/output.png)

## Methodology
Uses a parallel matrix (referred to as the "map") to hold indices which uniquely map clusters of the target value.

The map begins with all matching element positions set to -1, any other positions are set to 0.

The map is then iterated over. When a -1 is encountered, recursively set the index of this and all adjacent -1's to the same unique index.

Continue outer loop iteration to the end.

Print the results.

## Commentary
One interesting benefit of this method is elements that were already indexed into a group will not be deeply evaluated again by the main loop or the recursive check, as they are immediately changed from -1.


## Ideas for Improvement
[ ] Move matrix, width, and height to class
[ ] Find cross-platform identification solution (besides colors)