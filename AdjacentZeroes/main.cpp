// This application will find clusters of a target value in a matrix of values, uniquely coloring each cluster.

#include <cstdlib>
#include <iostream>

// Requires Windows I presume (for coloring output text)...
#include <windows.h>

// Function declarations (I prefer method definitions appearing below their use point)
int ** GenerateMatrix(int, int, int(*)(int,int));
void DestroyMatrix(int**, int);
int ** EvaluateMatrix(int**, int, int, int);
void Flood(int **, int, int, int, int, int);
void PrintMatrix(int**, int**, int, int);
void WaitForExit();


// Helper functions for providing initial values to a generated matrix

int GetZeroOrOne(int row, int column)
{
	return rand() % 2;
}

int GetZero(int row, int column)
{
	return 0;
}


int main()
{
	int width = 40;
	int height = 20;

	int ** matrix = GenerateMatrix(width, height, GetZeroOrOne);

	int ** map = EvaluateMatrix(matrix, width, height, 0);

	PrintMatrix(matrix, map, width, height);

	DestroyMatrix(matrix, height);

	DestroyMatrix(map, height);
	
	WaitForExit();

	return 0;
}

// Returns a 2D matrix of integers, initialized via provided function valueInit
int ** GenerateMatrix( int width, int height, int (*valueInit)(int,int) )
{
	int ** matrix = new int * [height];
	for (int row = 0; row < height; row++)
	{
		matrix[row] = new int[width];

		for (int column = 0; column < width; column++)
		{
			matrix[row][column] = valueInit(row, column);
		}
	}

	return matrix;
}

// The counterpoint for GenerateMatrix
void DestroyMatrix(int ** matrix, int height)
{
	for (int row = 0; row < height; row++)
	{
		delete matrix[row];
	}

	delete matrix;
}


// Parse the matrix to identify clusters of the target value
// Methodology: Uses a parallel matrix to hold indices mapping any clusters of the target value.
//              Init this matrix with 0 for all non-matching elements, and -1 for all matching
//              elements. This is then iterated over. When a -1 is encountered, recursively find
//              all adjacent -1's, changing them all to their own 'group' index. Continue
//              iteration to the end. Note one benefit of this method is elements that were already
//              indexed into a group will not be evaluated again by the main loop.
int ** EvaluateMatrix(int ** matrix, int width, int height, int target)
{
	// Init the map, mapping all non-target-value elements into group 0, and mapping all
	// target-value elements to group -1 (temporarily)...
	int ** map = new int *[height];
	for (int row = 0; row < height; row++)
	{
		map[row] = new int[width];

		for (int column = 0; column < width; column++)
		{
			map[row][column] = (matrix[row][column] == target ? -1 : 0);
		}
	}

	int groupIndex = 1;	// 0 is already used for non-target-value elements

	// Iterate over the map to find the next un-evaluated, target-value element
	for (int row = 0; row < height; row++)
	{
		for (int column = 0; column < width; column++)
		{
			if (map[row][column] == -1)
			{
				Flood(map, width, height, row, column, groupIndex);

				groupIndex++;
			}
		}
	}

	return map;
}

// Recursively assign the same group index to all neighboring target-value elements
void Flood(int ** map, int width, int height, int row, int column, int groupIndex)
{
	// Don't process anything other than un-processed target-value elements (in temp group -1).
	if (map[row][column] != -1)
		return;
		
	map[row][column] = groupIndex;
	
	if (row > 0)
		Flood(map, width, height, row - 1, column, groupIndex);
	if (row < height - 1)
		Flood(map, width, height, row + 1, column, groupIndex);
	if (column > 0)
		Flood(map, width, height, row, column - 1, groupIndex);
	if (column < width - 1)
		Flood(map, width, height, row, column + 1, groupIndex);
}

static inline void SetColor(int f, int b = 0)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (WORD)((b << 4) | f));
}

void PrintMatrix(int ** matrix, int ** map, int width, int height)
{
	for (int row = 0; row < height; row++)
	{
		for (int column = 0; column < width; column++)
		{
			SetColor(map[row][column] + 1);	// +1 to avoid black on black for non-target-value elements
			                                // (which is easier to read but not verify)
			std::cout << matrix[row][column];
		}

		std::cout << std::endl;
	}

	SetColor(7);
}

void WaitForExit()
{
	std::cout << std::endl << "Press <Enter> to exit...";
	std::cin.get();
}