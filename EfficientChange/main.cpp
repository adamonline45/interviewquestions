#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include "Coin.h"



void Intro()
{
	std::cout << "This app will give you the most efficient set of coins for a value." << std::endl;
}

int GetCents()
{
	int cents;
	std::cout << std::endl << "Enter a number of cents: ";
	std::cin >> cents;

	return cents;
}

std::vector<Coin> CalculateCoins( int cents )
{
	auto coins = std::vector<Coin>();
	
	for (size_t i = 0; i < Coin::coinTemplates.size(); i++)
	{
		auto currentCoinTemplate = Coin::coinTemplates[i];

		while (cents >= currentCoinTemplate.GetValue())
		{
			coins.push_back( currentCoinTemplate );
			cents -= currentCoinTemplate.GetValue();
		}
	}

	return coins;
}

void PrintCoins( std::vector<Coin> coins)
{
	int runningTally = 0;

	std::cout << std::endl;

	std::cout << std::setfill('_');
	std::cout << std::setw(15) << "Coin";
	std::cout << std::setw(6) << "Value";
	std::cout << std::setw(6) << "Tally" << std::endl;

	for (size_t i = 0; i < coins.size(); i++)
	{
		runningTally += coins[i].GetValue();
		
		std::cout << std::setfill(' ');
		std::cout << std::setw(15) << coins[i].GetName();
		std::cout << std::setw(6) << coins[i].GetValue();
		std::cout << std::setw(6) << runningTally << std::endl;
	}
}

void Wait()
{
	std::cout << std::endl << "Press <ENTER> to exit..." << std::endl;
	std::cin.get();
	std::cin.get();
}

int main()
{
	Intro();

	auto cents = GetCents();

	auto coins = CalculateCoins(cents);

	PrintCoins(coins);

	Wait();

	return 0;
}
