#include <string>

#include "Coin.h"

Coin::Coin(int value, std::string name)
{
	this->value = value;
	this->name = name;
}

Coin::~Coin()
{
}

int Coin::GetValue()
{
	return value;
}

std::string Coin::GetName()
{
	return name;
}

std::vector<Coin> Coin::coinTemplates
{
	// Make sure these are in descending order! Must include a coin with value of 1.

	Coin(100, "Dollar"),
	Coin(50, "Half-Dollar"),
	Coin(25, "Quarter"),
	Coin(10, "Dime"),
	Coin(5, "Nickel"),
	Coin(1, "Penny"),
};
