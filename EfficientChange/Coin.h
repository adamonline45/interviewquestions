#pragma once

#include <string>
#include <vector>

class Coin
{

private:
	int value;
	std::string name;

public:
	Coin(int, std::string);
	~Coin();

	int GetValue();
	std::string GetName();

	static std::vector<Coin> coinTemplates;
	
};

