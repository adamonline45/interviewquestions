# InterviewQuestions

## Projects
* **AdjacentZeroes** - Find and highlight clusters of a target value (0) within a matrix. (Windows only).
* **EfficientChangeSimple** - Calculate the least number of coins to meet some number of cents.
* **EfficientChange** - Same as EfficientChangeSimple, but over-engineered. WIP.
* **BinaryTree** - Implementation of a Binary Tree. Non-functional; basically a stub at this point.

Each directory may have its own README.md.

These are written using Visual Studio 2017.

You can build the solution as a whole, or one project at a time by right-clicking the project in the Solution Explorer and selecting "Build" from the context menu.

To run, you can find the compiled binary in your file system and run it as you would any other program, or you can right-click the project in the Solution Explorer and click "Set As StartUp Project," then click the Play button.